package com.matera.blog.restTeste;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.matera.blog.model.Post;
import com.matera.blog.repository.PostRepository;

@RestController
public class PostRest {
	@Autowired
    private PostRepository instanceRepository;

    public  PostRest(PostRepository repository) {
        this.instanceRepository = repository;
    }

    @RequestMapping(value = "/restPost", method = RequestMethod.GET)
    @ResponseBody
    public List<Post> findAll() {
        return this.instanceRepository.findAll();
    }	

    
}
